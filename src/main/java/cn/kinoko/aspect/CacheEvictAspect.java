package cn.kinoko.aspect;

import cn.kinoko.annotation.CacheEvict;
import cn.kinoko.service.RedisService;
import cn.kinoko.utils.SpelUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

/**
 * 分布式缓存删除切面
 * 策略：缓存双删
 * 注意：服务高可用的情况下很保险，但仍可能存在缓存不一致的情况，如redis宕机的等导致第二次删除失败
 *
 * @author kk
 */
@Aspect
@Component
public class CacheEvictAspect {

    @Autowired
    private RedisService redisService;

    @Around("@annotation(cacheEvict)")
    public Object around(ProceedingJoinPoint joinPoint, CacheEvict cacheEvict) throws Throwable {
        // 获取redisKey
        String redisKey = SpelUtil.getRedisKey(joinPoint, cacheEvict.key(), cacheEvict.params());
        // 执行业务方法
        Object target = joinPoint.proceed();
        // 删除缓存
        delKey(joinPoint, cacheEvict, redisKey);
        // 双删
        if (cacheEvict.waitTime() != -1) {
            // 提出waitTime防止注解对象在切面执行后被回收导致空指针
            int waitTime = cacheEvict.waitTime();
            // 异步删除缓存
            CompletableFuture.runAsync(() -> {
                // 等待时间
                LockSupport.parkNanos(TimeUnit.MILLISECONDS.toNanos(waitTime));
                delKey(joinPoint, cacheEvict, redisKey);
            });
        }
        return target;
    }

    /**
     * 删除缓存
     * @param joinPoint 业务方法
     * @param cacheable 缓存注解
     * @param redisKey redisKey
     */
    private void delKey(ProceedingJoinPoint joinPoint, CacheEvict cacheable, String redisKey) {
        // 检查条件，删除缓存
        if (!SpelUtil.getBoolean(joinPoint, cacheable.unless(), false)) {
            redisService.del(redisKey);
        }
    }

}
