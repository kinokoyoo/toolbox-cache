package cn.kinoko.aspect;

import cn.kinoko.annotation.CachePut;
import cn.kinoko.service.RedisService;
import cn.kinoko.utils.SpelUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 分布式缓存插入切面
 *
 * @author kk
 */
@Aspect
@Component
public class CachePutAspect {

    @Autowired
    private RedisService redisService;

    @Around("@annotation(cachePut)")
    public Object around(ProceedingJoinPoint joinPoint, CachePut cachePut) throws Throwable {
        // 执行业务方法
        Object target = joinPoint.proceed();
        // 检查条件，加入缓存
        if (target != null && !SpelUtil.getBoolean(joinPoint, cachePut.unless(), false)) {
            // 获取redisKey
            String redisKey = SpelUtil.getRedisKey(joinPoint, cachePut.key(), cachePut.params());
            // 设置缓存
            if (cachePut.expire() == -1) {
                redisService.set(redisKey, target);
            } else {
                redisService.set(redisKey, target, SpelUtil.toDuration(cachePut.expire(), cachePut.timeUnit()));
            }
        }
        return target;
    }

}
