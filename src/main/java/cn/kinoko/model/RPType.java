package cn.kinoko.model;

/**
 * @author kk
 */
public enum RPType {

    STR("string"),
    SET("set"),
    LIST("list"),
    HASH("hash"),
    ZSET("zset"),
    NONE("none");

    RPType(String type) {
        this.type = type;
    }

    private final String type;

    public String getType() {
        return type;
    }

    public static RPType get(String type) {
        for (RPType t : RPType.values()) {
            if (t.getType().equals(type)) {
                return t;
            }
        }
        return NONE;
    }
}
