package cn.kinoko.annotation;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

/**
 * 分布式缓存删除注解
 * @author kk
 */
@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface CacheEvict {

    // 缓存的key
    String key() default "";
    // 缓存的key参数
    String params() default "";
    // 不加入缓存的条件
    String unless() default "";
    // 双删策略等待时间(ms)
    int waitTime() default -1;

}
