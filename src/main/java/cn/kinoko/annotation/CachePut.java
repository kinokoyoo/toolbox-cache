package cn.kinoko.annotation;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

/**
 * 分布式缓存插入注解
 * @author kk
 */
@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface CachePut {

    // 缓存的key
    String key() default "";
    // 缓存的key参数
    String params() default "";
    // 不加入缓存的条件
    String unless() default "";
    // 过期时间
    int expire() default -1;
    // 时间单位
    TimeUnit timeUnit() default TimeUnit.SECONDS;

}
