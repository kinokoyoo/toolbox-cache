package cn.kinoko.annotation;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

/**
 * 分布式锁注解
 * @author kinoko
 */
@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface DistributedLock {
    // 锁的key
    String key() default "";
    // 锁的key参数
    String params() default "";
    // 锁的过期时间
    int expire() default 10;
    // 自动释放锁
    boolean autoRelease() default true;
    // 异常描述
    String errorDesc() default "系统繁忙，请稍后再试";
    // 等待时间
    int waitTime() default 1;
    // 时间单位
    TimeUnit timeUnit() default TimeUnit.SECONDS;

}