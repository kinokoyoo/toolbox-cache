package cn.kinoko.annotation;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

/**
 * 分布式缓存注解
 * @author kk
 */
@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface Cacheable {

    // 缓存的key
    String key() default "";
    // 缓存的key参数
    String params() default "";
    // 不加入缓存的条件
    String unless() default "";
    // 过期时间
    int expire() default -1;
    // 时间单位
    TimeUnit timeUnit() default TimeUnit.SECONDS;
    // 开启类型匹配
    boolean typeMatch() default false;
    // 开启布隆校验
    boolean enableBloom() default false;
    String bloomHitFail() default "数据命中失败";
}
