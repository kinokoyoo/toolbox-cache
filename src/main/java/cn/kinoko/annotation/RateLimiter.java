package cn.kinoko.annotation;

import org.redisson.api.RateIntervalUnit;
import org.redisson.api.RateType;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

/**
 * 时间窗口限流注解
 *
 * @author kinoko
 */
@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface RateLimiter {

    // 限流key
    String key() default "";
    // 限流粒度
    Granularity granularity() default Granularity.NO_LIMIT;
    // 限流错误描述
    String errorDesc() default "网络繁忙，请稍后再试";
    // 限流类型
    RateType rateType() default RateType.OVERALL;
    // 速率（产生几个令牌）
    int rate() default 1;
    // 速率间隔
    int rateInterval() default 1;
    // 限流时间单位
    RateIntervalUnit intervalUnit() default RateIntervalUnit.SECONDS;
    // 限流等待时间
    int timeout() default -1;
    // 时间单位
    TimeUnit timeUnit() default TimeUnit.SECONDS;

    enum Granularity {

        /**
         * 不限制粒度
         */
        NO_LIMIT,
        /**
         * 按用户限流
         */
        USER,
        /**
         * 按IP限流
         */
        IP
    }

}
