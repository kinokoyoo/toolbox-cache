package cn.kinoko.component;

import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.io.support.SpringFactoriesLoader;
import org.springframework.core.type.AnnotationMetadata;

import java.util.List;

/**
 * 组件加载器
 *
 * @author kinoko
 */
public class ComponentImportSelector implements ImportSelector {

    @Override
    public String[] selectImports(AnnotationMetadata importingClassMetadata) {
        // 从spring.factory中获取组件
        List<String> names = SpringFactoriesLoader.loadFactoryNames(ComponentImportSelector.class, null);
        return names.toArray(new String[0]);
    }
}