package cn.kinoko.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author kinoko
 */
@Setter
@Getter
@ConfigurationProperties(prefix = "spring.redisson")
@Configuration
public class ConfigProperties {

    private String file;
    private boolean enablePrefix;
    private boolean typeMatch;
    private BloomFilterConfig bloomFilter;

    public ConfigProperties() {
        this.file = "classpath:redisson.yaml";
        this.enablePrefix = false;
        this.typeMatch = false;
        this.bloomFilter = new BloomFilterConfig();
    }

    @Getter
    @Setter
    public static class BloomFilterConfig {

        private String key;
        private boolean enable;
        private double tolerateError;
        private int releaseRetry;

        public BloomFilterConfig() {
            this.key = "boolean-filter";
            this.enable = false;
            this.tolerateError = 0.05;
            this.releaseRetry = 10;
        }
    }

}