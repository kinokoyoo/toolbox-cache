package cn.kinoko.utils;

import java.lang.reflect.Type;

/**
 * 类型检查工具类
 * @author kinoko
 */
public class TypeChecker {

    // 定义Java基本类型与对应的包装类
    private static final Class<?>[] PRIMITIVE_TYPES = {
            Boolean.class, Character.class,
            Byte.class, Short.class, Integer.class, Long.class,
            Float.class, Double.class, String.class
    };

    /**
     * 判断是否是基本类型或包装类型
     * @param type 类型
     * @return true: 是基本类型或包装类型
     */
    public static boolean isWrapperType(Type type) {
        if (type instanceof Class<?>) {
            Class<?> clazz = (Class<?>) type;
            for (Class<?> primitiveType : PRIMITIVE_TYPES) {
                if (primitiveType.equals(clazz)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 判断是否是基本类型或包装类型
     * @param obj 对象
     * @return true: 是基本类型或包装类型
     */
    public static boolean isPrimitiveOrWrapper(Object obj) {
        return obj != null && isWrapperType(obj.getClass());
    }
}